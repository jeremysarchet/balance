class AddReconciledOnToAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :accounts, :reconciled_on, :date
  end
end
