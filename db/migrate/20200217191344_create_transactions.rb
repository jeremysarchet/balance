class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.date :date
      t.decimal :amount, precision: 8, scale: 2
      t.string :description
      t.references :sender
      t.references :receiver
      t.references :category
      t.references :subcategory

      t.timestamps
    end
  end
end
