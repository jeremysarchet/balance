class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.string :name
      t.boolean :owned
      t.boolean :retirement
      t.decimal :initial, precision: 8, scale: 2

      t.timestamps
    end
  end
end
