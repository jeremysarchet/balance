class TightenUpAccounts < ActiveRecord::Migration[7.0]
  def change
    change_column :accounts, :owned, :boolean, null: false, default: true
    change_column :accounts, :retirement, :boolean, null: false, default: false
    change_column :accounts, :initial, :decimal, precision: 8, scale: 2, default: 0.00
    add_index :accounts, :name, unique: true
  end
end
