# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

expensesAccount = Account.create!(
  name: 'Expenses',
  owned: false,
);

incomeAccount = Account.create!(
  name: 'Income',
  owned: false,
);

bankAccount = Account.create!(
  name: 'Bank',
);

jobCategory = Category.create!(
  name: 'job'
);

groceriesCategory = Category.create!(
  name: 'groceries'
);


Transaction.create!([
  {
    date: '2023-12-01',
    amount: 4000,
    description: 'A paycheck',
    sender: incomeAccount,
    receiver: bankAccount,
    category: jobCategory,
  },
  {
    date: '2023-12-05',
    amount: 42,
    description: 'supermarket',
    sender: bankAccount,
    receiver: expensesAccount,
    category: groceriesCategory,
  },
])

puts 'Seeding completed'
