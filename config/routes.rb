Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get 'welcome/index'

  get 'accounts/balances'

  if Mode.demo?
    resources :accounts, except: [:new, :edit, :destroy]
    resources :categories, except: [:new, :edit, :destroy]
    resources :subcategories, except: [:new, :edit, :destroy]
    resources :transactions
  else
    resources :accounts
    resources :categories
    resources :subcategories
    get 'transactions/upload'
    resources :transactions do
      collection { post :import }
    end
  end

  root 'accounts#balances'
end
