class Category < ApplicationRecord
  has_many :transactions, :dependent => :restrict_with_exception
  has_many :subcategories
end
