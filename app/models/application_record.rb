class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def ordering_attribute
    if self.respond_to?("name")
      "name"
    elsif self.respond_to?("date")
      "date"
    else
      "id"
    end
  end

  def next
    a = ordering_attribute
    self.class.where("#{a} > ?", self.send(a)).\
    order("#{a} ASC").first ||
    self.class.order("#{a} ASC").first
  end

  def previous
    a = ordering_attribute
    self.class.where("#{a} < ?", self.send(a)).\
    order("#{a} DESC").first ||
    self.class.order("#{a} DESC").first
  end

end
