class Transaction < ApplicationRecord
  belongs_to :sender, class_name: 'Account'
  belongs_to :receiver, class_name: 'Account'

  belongs_to :category
  belongs_to :subcategory, optional: true

  validates :date, presence: true
  validates :amount, presence: true
  validates :description, presence: true, length: { minimum: 3 }
  validates :sender, presence: true
  validates :receiver, exclusion: { in: lambda{ |transaction| [transaction.sender] } }

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      hash = row.to_hash

      hash["sender"] = Account.find_or_create_by!(name: hash["sender"],
                                                  owned: hash["sender"] != "Income",
                                                  retirement: false,
                                                  initial: 0.0)

      hash["receiver"] = Account.find_or_create_by!(name: hash["receiver"],
                                                    owned: hash["receiver"] != "Expenses",
                                                    retirement: false,
                                                    initial: 0.0)

      hash["category"] = hash["category"] || "Uncategorized"
      hash["category"] = Category.find_or_create_by(name: hash["category"])

      if hash["subcategory"].nil? || hash["subcategory"].empty?
        hash.delete("subcategory")
      else
        hash["subcategory"] = Subcategory.find_or_create_by({name: hash["subcategory"], category: hash["category"]})
      end

      Transaction.create hash
    end
  end

  def self.to_csv
    CSV.generate do |csv|
      # csv << column_names
      csv << ["date", "amount", "description", "sender", "receiver", "category", "subcategory"]
      all.each do |transaction|
        # csv << transaction.attributes.values_at(*column_names)
        csv << [transaction.date,
                transaction.amount,
                transaction.description,
                transaction.sender.name,
                transaction.receiver.name,
                transaction.category.name,
                !transaction.subcategory.nil? ? transaction.subcategory.name : "",
                ]
      end
    end
  end

  def self.search(search)
    if search
      where("LOWER(description) LIKE ?", "%#{search.downcase}%")
    else
      where(nil)
    end
  end

  def self.income
    includes(:sender).where(accounts: {owned: false})
  end

  def self.expenses
    includes(:receiver).where(accounts: {owned: false})
  end

end
