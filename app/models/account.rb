class Account < ApplicationRecord
  # Accounts have transactions associated with them either as an input or an
  # output.
  # A transaction is an input to a given account if the transaction's
  # receiver_id is the id of this account.
  # A transaction is an output from a given account if the transaction's
  # sender_id is the id of the given account.
  # Don't permit deletion of an account if there are dependent transactions
  # Can't have any transactions referencing a non-existent account
  has_many :inputs, :class_name => 'Transaction', :foreign_key => 'receiver_id', :dependent => :restrict_with_exception
  has_many :outputs, :class_name => 'Transaction', :foreign_key => 'sender_id', :dependent => :restrict_with_exception

  # Ensure the account meets requirements for its fields
  # Validate a boolean
  # https://guides.rubyonrails.org/active_record_validations.html#presence
  validates :name, presence: true, length: {minimum: 3}
  validates :owned, inclusion: { in: [true, false] }
  validates :retirement, inclusion: { in: [true, false] }
  validates :initial, numericality: true

  def self.to_csv
    CSV.generate do |csv|
      # Manually populate the csv header row.
      # Alternatively, to do all the fields of the record:
      # https://apidock.com/rails/ActiveRecord/ModelSchema/ClassMethods/column_names
      # csv << column_names
      csv << ["name", "owned", "retirement", "initial"]
      all.each do |account|
        # Populate the row with the corresponding record fields
        csv << [account.name,
                account.owned,
                account.retirement,
                account.initial]
        # Alternatively, unpack all column names
        # csv << transaction.attributes.values_at(*column_names)
      end
    end
  end

  def balance
    # Returns the computed sum of the initial value plus all the inputs
    # minus all the outputs
    self.initial + self.inputs.sum(:amount) - self.outputs.sum(:amount)
  end

end
