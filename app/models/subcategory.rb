class Subcategory < ApplicationRecord
  belongs_to :category
  has_many :transactions, :dependent => :restrict_with_exception
end
