class AccountsController < ApplicationController
  def index
    # Get accounts by relevant designations, ordered by name
    @accounts = Account.all.order(name: :asc)
    @owned_accounts = Account.where(owned: true).order(name: :asc)
    @external_accounts = Account.where(owned: false).order(name: :asc)
    @non_retirement_accounts = @owned_accounts.where(retirement: false).order(name: :asc)
    @retirement_accounts = @owned_accounts.where(retirement: true).order(name: :asc)

    # Go through each account and compute net activity and populate a hash to
    # pass to the view
    # QUESTION: Is there a tidier way to do this at the model level? E.g.
    # have an instance method `net_activity` which would then seemingly need
    # access to the filter. The below article seems relevant to return to
    # https://www.justinweiss.com/articles/search-and-filter-rails-models-without-bloating-your-controller/
    @nets = {}
    @accounts.each do |account|
      # The net activity (across the filtered transactions) is the sum of
      # the inputs minus the sum of the outputs
      input = filter(account.inputs).sum(:amount)
      output = filter(account.outputs).sum(:amount)
      sum = input - output
      @nets[account.id] = account.owned ? sum : -sum
    end

    # Provide for downloading all account data as a csv file
    # see: https://www.codementor.io/@victor_hazbun/export-records-to-csv-files-ruby-on-rails-vda8323q0
    respond_to do |format|
      format.html
      format.csv { send_data @accounts.to_csv }
    end

  end

  def balances
    # Special action to view the current balance of an account, computed over
    # all transactions, starting from the initial value and summing all the
    # inputs and outputs.
    # The balance of an account only makes sense where the account is owned

    # Get all owned accounts and split them based on the retirement designation
    @owned_accounts = Account.where(owned: true).order(name: :asc)
    @non_retirement_accounts = @owned_accounts.where(retirement: false).order(name: :asc)
    @retirement_accounts = @owned_accounts.where(retirement: true).order(name: :asc)

    # Iterate through all the owned accounts and sum their balances to compute
    # net worth to pass to the view
    @net_worth = 0
    @owned_accounts.each do |account|
      initial = account.initial
      @net_worth += account.balance
    end
  end

  def show
    # Show a single account and relevant values and associated transactions
    @account = Account.find(params[:id])

    # Information based on filtered transactions associated with the account
    @count = filter(@account.inputs.or(@account.outputs)).count
    @input = filter(@account.inputs).sum(:amount)
    @output = filter(@account.outputs).sum(:amount)
    sum = @input - @output

    # Invert the net activity of the account if it is not owned
    # this is because the perspective to you is flipped for external accounts
    @net = @account.owned ? sum : -sum

    # Get all relevant transactions to this account whether they be inputs or
    # outputs
    combined = combined = @account.inputs.or(@account.outputs)
    @transactions = filter(combined)
  end

  def new
    # Create a new account via html form
    @account = Account.new
  end

  def edit
    # Edit the given account by id via html form
    @account = Account.find(params[:id])
  end

  def create
    # Attempt to save a new account to the database. if there are problems,
    # render the form again (and the view is to present the problems)
    @account = Account.new(account_params)

    if @account.save
      redirect_to @account
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def update
    # Modify the account by id via html form and attempt to save it to the
    # database. If there are problems render the form again and show them.
    @account = Account.find(params[:id])

    if @account.update(account_params)
      redirect_to @account
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    # Remove an account record from the database. Restrict destruction in the
    # case where an account has transactions. Don't want to leave any
    # transactions pointing to non-existent accounts.
    @account = Account.find(params[:id])

    # Try and destroy the account and prepare a success flash message
    begin
      @account.destroy
      flash[:success] = "Successfully deleted account: #{@account.name}"
      redirect_to accounts_path, status: :see_other
    # Catch for the case where destruction isn't allowed and prepare a flash
    rescue ActiveRecord::DeleteRestrictionError
      flash[:error] = "Cannot delete this account because it has associated
      transactions"
      redirect_to account_path
    end
  end

  private
    def account_params
      # Stipulate the parameters of an account explicitly
      params.require(:account).permit(:name, :owned, :retirement, :initial, :reconciled_on)
    end
end
