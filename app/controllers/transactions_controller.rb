class TransactionsController < ApplicationController
  def index
    # Get all transactions subject to filtering
    # Filtering includes: date range, search, and
    # QUESTION: at what point might it become poor performing to rely merely on
    # the SQL LIKE operator for search
    # https://en.wikipedia.org/wiki/Inverted_index
    # https://github.com/Casecommons/pg_search
    @transactions = filter(Transaction)

    # Get the count, and transactions by income vs expenses and compute net
    # activity across the collection of transactions
    @count = @transactions.count
    @income = @transactions.income.sum(:amount)
    @expenses = @transactions.expenses.sum(:amount)
    @net = @income - @expenses

    # Support downloading transaction data as csv
    respond_to do |format|
      format.html
      format.csv { send_data @transactions.to_csv }
    end

  end

  def show
    @transaction = Transaction.find(params[:id])
  end

  def new
    @transaction = Transaction.new
  end

  def import
    # Action to pass the csv file via query parameter and call the import
    # method of the Transaction model
    Transaction.import(params[:file])
    redirect_to transactions_path, notice: "Transaction CSV data imported"
  end

  def upload
  end

  def edit
    @transaction = Transaction.find(params[:id])
  end

  def create
    @transaction = Transaction.new(transaction_params)

    if @transaction.save
      redirect_to @transaction
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def update
    @transaction = Transaction.find(params[:id])

    if @transaction.update(transaction_params)
      redirect_to @transaction
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    @transaction = Transaction.find(params[:id])
    @transaction.destroy
    flash[:success] = "Successfully deleted transaction dated: #{@transaction.date}"

    redirect_to transactions_path, status: :see_other
  end

  private
    def transaction_params
      # Constrain valid parameters pertinent to a transaction
      params.require(:transaction).permit(:date,
                                          :amount,
                                          :description,
                                          :sender_id,
                                          :receiver_id,
                                          :category_id,
                                          :subcategory_id)
    end

end
