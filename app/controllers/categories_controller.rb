class CategoriesController < ApplicationController
  def index
    # Prepare collection of categories ordered by name
    @categories = Category.all.order(name: :asc)

    # Iterate through the categories and compute net activity for each
    @nets = {}
    @categories.each do |category|
      # Get the transactions of the category subject to the current filter
      transactions = filter(category.transactions)
      # Net activity is sum(income transactions) - sum(expense transactions)
      income = transactions.income.sum(:amount)
      expenses = transactions.expenses.sum(:amount)
      @nets[category.id] = income - expenses
    end
  end

  def show
    # Show the given category and pertinent information based on id
    @category = Category.find(params[:id])

    # Perform computations on the transactions of the category and
    # TODO: investigate moving this into the Transaction model instead which
    # may be a good way to reduce duplication between this and the index method
    @transactions = filter(@category.transactions)
    @count = @transactions.count
    @income = @transactions.income.sum(:amount)
    @expenses = @transactions.expenses.sum(:amount)
    @net = @income - @expenses
  end

  def new
    @category = Category.new
  end

  def edit
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to @category
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def update
    @category = Category.find(params[:id])

    if @category.update(category_params)
      redirect_to @category
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    # Remove an category record from the database. Restrict destruction in the
    # case where it has transactions. Don't want to leave any
    # transactions pointing to non-existent categories.
    @category = Category.find(params[:id])
    begin
      @category.destroy
      flash[:success] = "Successfully deleted category: #{@category.name}"
      redirect_to categories_path, status: :see_other
    rescue ActiveRecord::DeleteRestrictionError
      flash[:error] = "Cannot delete this category because it has associated transactions"
      redirect_to category_path
    end
  end

  private
    def category_params
      params.require(:category).permit(:name)
    end
end
