class ApplicationController < ActionController::Base
  # Setup basic authentication based on environment variables for the whole
  # application, except in the case of demo mode.

  unless ENV["BALANCE_USERNAME"] && ENV["BALANCE_PASSWORD"]
    raise 'Missing `BALANCE_USERNAME` and/or `BALANCE_PASSWORD` environment variables'
  end

  unless Mode.demo?
    http_basic_authenticate_with name: ENV["BALANCE_USERNAME"], password: ENV["BALANCE_PASSWORD"]
  end

  # Most of the functionality in this class pertains to transactions, in
  # particular, sorting and filtering them.
  # QUESTION: would it be better placed in the transaction controller? I
  # originally put it here as many controllers utilize these methods, but they
  # may be utimately best in the transaction model.
  # http://railscasts.com/episodes/228-sortable-table-columns
  # http://railscasts.com/episodes/240-search-sort-paginate-with-ajax

  # Allow these methods to be called from the application helper
  helper_method :sort_column, :sort_direction

  def filter(query)
    # Given a query, add the necessary inclusion if any and perform filtering
    # based on search, date range and column-based sorting.
    query.includes(sort_inclusion).
    search(params[:search]).
    where(date: start_date..end_date).
    order(sort_by + " " + sort_direction)
  end

  def start_date
    # Returns the start date for a date range search if present, otherwise
    # returns a given time a long time ago
    !params[:start_date].blank? ? params[:start_date] : 10.years.ago
  end

  def end_date
    # Returns the end date for a date range search if present, otherwise
    # returns the present time
    !params[:end_date].blank? ? params[:end_date] : 0.days.ago
  end

  def sort_column
    # Return the current sort via query parameter if present, otherwise default
    # to returning "date"
    ["date",
      "amount",
      "description",
      "sender",
      "receiver",
      "category",
      "subcategory",
    ].include?(params[:sort]) ? params[:sort] : "date"
  end

  def sort_direction
    # Return the sort direction via query parameter if present, otherwise
    # return "desc" as the default
    ["asc", "desc"].include?(params[:direction]) ? params[:direction] : "desc"
  end

  def sort_by
    # Gets the name of the current column to sort by (or the default) by calling
    # `sort_column` and returns the corresponding string to pass to the `order`
    # function to order the active record query.
    {"date" => "date",
     "amount" => "amount",
     "description" => "description",
     "sender" => "accounts.name",
     "receiver" => "accounts.name",
     "category" => "categories.name",
     "subcategory" => "subcategories.name"
     }[sort_column]
  end

  def sort_inclusion
    # Gets the name of the current column to sort by (or the default) by calling
    # `sort_column` and returns the corresponding string to pass to the `order`
    # function to order the active record query.
    {"date" => nil,
     "amount" => nil,
     "description" => nil,
     "sender" => :sender,
     "receiver" => :receiver,
     "category" => :category,
     "subcategory" => :subcategory
     }[sort_column]
  end

end
