class SubcategoriesController < ApplicationController
  def index
    @subcategories = Subcategory.all.order(name: :asc)

    # Go through all the transactions associated with this subcategory and
    # compute the net activity through that category
    # TODO: see about moving this to the Transaction model
    @nets = {}
    @subcategories.each do |subcategory|
      transactions = filter(subcategory.transactions)
      income = transactions.income.sum(:amount)
      expenses = transactions.expenses.sum(:amount)
      @nets[subcategory.id] = income - expenses
    end
  end

  def show
    # TODO: see about de-duplication of this from the index method
    @subcategory = Subcategory.find(params[:id])

    @transactions = filter(@subcategory.transactions)
    @count = @transactions.count
    @income = @transactions.income.sum(:amount)
    @expenses = @transactions.expenses.sum(:amount)
    @net = @income - @expenses
  end

  def new
    @subcategory = Subcategory.new
  end

  def edit
    @subcategory = Subcategory.find(params[:id])
  end

  def create
    @subcategory = Subcategory.new(subcategory_params)

    if @subcategory.save
      redirect_to @subcategory
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def update
    @subcategory = Subcategory.find(params[:id])

    if @subcategory.update(subcategory_params)
      redirect_to @subcategory
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    # Remove an subcategory record from the database. Restrict destruction in the
    # case where it has transactions. Don't want to leave any
    # transactions pointing to non-existent subcategories.
    @subcategory = Subcategory.find(params[:id])
    begin
      @subcategory.destroy
      flash[:success] = "Successfully deleted subcategory: #{@subcategory.name}"
      redirect_to subcategories_path, status: :see_other
    rescue ActiveRecord::DeleteRestrictionError
      flash[:error] = "Cannot delete this subcategory because it has associated transactions"
      redirect_to subcategory_path
    end
  end

  private
    def subcategory_params
      params.require(:subcategory).permit(:name, :category_id)
    end
end
