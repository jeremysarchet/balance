module ApplicationHelper

  def sortable(column, title = nil)
    # Sortable table columns from:
    # http://railscasts.com/episodes/228-sortable-table-columns
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    # Pass the sort and direction parameters in, merging with existing
    # parameters and setup a link with the css class to indicate that the column
    # is the one currently responsible for the sort
    link_to title, params.permit(:id, :search, :start_date, :end_date).merge(:sort => column, :direction => direction), {:class => css_class}
  end

end
