# Balance

## Local development

1. Get Postgres going with Docker:
  - `docker run --name balance-development -e POSTGRES_USER=balance -e POSTGRES_PASSWORD=dev -p 5432:5432 -d postgres`
2. Check connecting to it with `psql`:
  - `PGPASSWORD=dev psql -h localhost -p 5432 -U balance -d postgres`
  - Gotta connect to it like so: `\c balance_development`
3. Get it going with Rails:
  - `RAILS_ENV=development rails db:create`
  - `RAILS_ENV=development rails db:migrate`
  - `RAILS_ENV=development rails db:seed`
4. Start the server:
  - BALANCE_USERNAME=dev BALANCE_PASSWORD=dev rails server -p 3001
5. Browse the db with rails console
  - `RAILS_ENV=development rails console`
  - e.g. `Account.all`

## Heroku

- `heroku apps`
- `heroku info my-app`
